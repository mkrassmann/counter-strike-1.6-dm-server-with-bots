# Counter-Strike 1.6 DM Server with Bots

This Dockerfile provides a simple-to-setup CS 1.6 server with
preinstalled bots. It'll be a perfect training environment for aim training.
